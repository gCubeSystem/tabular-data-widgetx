/**
 * 
 */
package org.gcube.portlets.user.tdwx.server.datasource;

/**
 * Constants for sorting direction.
 * @author "Federico De Faveri defaveri@isti.cnr.it"
 */
public enum Direction {
	
	/**
	 * Ascending. 
	 */
	ASC,
	
	/**
	 * Descending. 
	 */
	DESC;
}
